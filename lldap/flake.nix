{
  inputs = {
    utils.url = "github:numtide/flake-utils";
  };
  outputs = { self, nixpkgs, utils }: utils.lib.eachDefaultSystem (system:
    let
      pkgs = nixpkgs.legacyPackages.${system};
      basePkg = pkgs.rustPlatform.buildRustPackage rec {
        pname = "lldap";
        version = "0.4.0";
        src = pkgs.fetchFromGitHub {
          owner = "nitnelave";
          repo = "lldap";
          rev = "v${version}";
          hash = "sha256-lw4wo1YY9O3ZK+7oUjU8gLcAddu7Og/0LpbEzIOUTJE=";
        };
        cargoSha256 = "sha256-sM69ltYg+AyhNvjbD/qAdO4vJqIqDbUdh7HBF524C9M=";
        nativeBuildInputs = with pkgs; [ perl ];
        #preBuild = ''
        #  # rewritten app/build.sh
        #  cd app
        #  wasm-pack build --target web
        #  rollup ./main.js --format iife --file ./pkg/bundle.js --globals bootstrap:bootstrap
        #  cd -
        #'';
        postInstall = ''
          cp -r app $out/app
        '';
        meta = with pkgs.lib; {
          description = "";
          homepage = "https://github.com/nitnelave/lldap";
          license = licenses.gpl3;
          platforms = platforms.unix;
        };
      };
    in rec {
      nixosModules.lldap = { config, lib, pkgs, ... }: let cfg = config.services.lldap; in {
        options.services.lldap = {
          enable = lib.mkEnableOption "";
          httpPort = lib.mkOption {
            default = 17170;
            type = lib.types.port;
          };
          ldapPort = lib.mkOption {
            default = 3890;
            type = lib.types.port;
          };
          jwtSecretFile = lib.mkOption {
            type = lib.types.path;
            default = "/var/lib/lldap/jwt_secret_key";
          };
          baseDn = lib.mkOption {
            type = lib.types.str;
            example = "dc=example,dc=com";
          };
          admin = {
            username = lib.mkOption {
              type = lib.types.str;
              default = "admin";
            };
            email = lib.mkOption {
              type = lib.types.str;
              default = "admin@example.com";
            };
          };
          httpUrl = lib.mkOption {
            type = lib.types.str;
            example = "https://ldap.example.com";
          };
          databaseUrl = lib.mkOption {
            type = lib.types.str;
            default = "sqlite:///var/lib/lldap/users.db?mode=rwc";
          };
          keyFile = lib.mkOption {
            type = lib.types.path;
            default = "/var/lib/lldap/private_key";
          };
        };
        config = lib.mkIf cfg.enable {
          systemd.services.lldap = let configFile = (pkgs.formats.toml {}).generate "lldap-config.toml" {
            ldap_port = cfg.ldapPort;
            http_port = cfg.httpPort;
            http_url = cfg.httpUrl;
            ldap_base_dn = cfg.baseDn;
            ldap_user_dn = cfg.admin.username;
            ldap_user_email = cfg.admin.email;
            database_url = cfg.databaseUrl;
            key_file = cfg.keyFile;
          }; in {
            after = [ "network.target" ];
            serviceConfig = {
              ExecStart = ''
                ${basePkg}/bin/lldap run --config-file ${configFile}
              '';
              EnvironmentFile = cfg.jwtSecretFile;
              Restart = lib.mkDefault "always";
              PrivateTmp = lib.mkDefault true;
              WorkingDirectory = lib.mkDefault "/var/lib/lldap";
              User = lib.mkDefault "lldap";
              Group = lib.mkDefault "lldap";
            };
            wantedBy = [ "multi-user.target" ];
          };
          users.groups.lldap = {};
          users.users.lldap = {
            createHome = true;
            group = "lldap";
            home = "/var/lib/lldap";
            isSystemUser = true;
          };
        };
      };
      packages = {
        default = basePkg;
        lldap = basePkg;
      };
    });
}
