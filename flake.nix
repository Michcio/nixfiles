{
  description = "Michcio flavoured NixOS";
  inputs = {
    utils.url = "github:numtide/flake-utils";
    neovim-michcio = {
      url = "./neovim-michcio";
      inputs.nixpkgs.follows = "nixpkgs";
      inputs.utils.follows = "utils";
    };
    fronius-exporter = {
      url = "./fronius-exporter";
      inputs.nixpkgs.follows = "nixpkgs";
      inputs.utils.follows = "utils";
    };
    ecosol-exporter = {
      url = "./ecosol-exporter";
      inputs.nixpkgs.follows = "nixpkgs";
      inputs.utils.follows = "utils";
    };
    lldap = {
      url = "./lldap";
      inputs.nixpkgs.follows = "nixpkgs";
      inputs.utils.follows = "utils";
    };
  };
  outputs = { self, nixpkgs, utils, neovim-michcio, fronius-exporter, ecosol-exporter, lldap }:
    let
      children = {
        inherit neovim-michcio fronius-exporter ecosol-exporter lldap;
      };
      concatAttrs = attrs:
        builtins.zipAttrsWith (name: values: {
          inherit name;
          value = builtins.head values;
        }) attrs;
    in
    utils.lib.eachDefaultSystem (system: {
      packages = utils.lib.flattenTree (builtins.mapAttrs (name: child: child.packages.${system} or {}) children);
      nixosModules = builtins.mapAttrs (name: child: child.nixosModules.${system}.${name} or {}) children;
      lib = builtins.mapAttrs (name: child: child.lib.${system} or {}) children;
    });
}
