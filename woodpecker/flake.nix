{
  description = "Setup for Woodpecker CI";
  inputs = {
    utils.url = "github:numtide/flake-utils";
  };
  outputs = { self, nixpkgs, utils }:
    utils.lib.eachDefaultSystem (system:
      let
        pkgs = nixpkgs.legacyPackages.${system};
      in
      rec {
        nixosModules.woodpecker = { config, lib, pkgs, ... }:
        let
          cfg = config.services.woodpecker;
        in
          {
            options.services.woodpecker = {
              enable = lib.mkEnableOption "install Woodpecker CI";
              host = lib.mkOption {
                type = lib.types.str;
                example = "http://woodpecker.example.org";
              };
              openRegistration = lib.mkOption {
                type = lib.types.bool;
                default = false;
              };
              agentSecretFile = lib.mkOption {
                type = lib.types.path;
              };
              extraConfig = lib.mkOption {
                type = lib.types.attrs;
                default = {};
              };
              gitea = {
                enable = lib.mkEnableOption "use gitea as vcs";
                url = lib.mkOption {
                  type = lib.types.str;
                };
                clientId = lib.mkOption {
                  type = lib.types.str;
                };
                secretFile = lib.mkOption {
                  type = lib.types.path;
                };
              };
            };
            config = lib.mkIf cfg.enable {
              virtualisation = {
                oci-containers = {
                  backend = "podman";
                  containers = {
                    woodpecker-server = {
                      image = "woodpeckerci/woodpecker-server:latest";
                      volumes = [
                        "woodpecker-server-data:/var/lib/woodpecker/"
                        "${cfg.agentSecretFile}:/agentsecretfile:ro"
                      ] ++ lib.optionals cfg.gitea.enable [
                        "${cfg.gitea.secretFile}:/giteasecretfile:ro"
                      ];
                      environment = {
                        WOODPECKER_OPEN = "${toString cfg.openRegistration}";
                        WOODPECKER_HOST = cfg.host;
                        WOODPECKER_AGENT_SECRET_FILE = "/agentsecretfile";
                      } // lib.optionalAttrs cfg.gitea.enable {
                        WOODPECKER_GITEA = "true";
                        WOODPECKER_GITEA_URL = cfg.gitea.url;
                        WOODPECKER_GITEA_CLIENT = cfg.gitea.clientId;
                        WOODPECKER_GITEA_SECRET_FILE = "/giteasecretfile";
                      } // cfg.extraConfig;
                    };
                    woodpecker-agent = {
                      image = "woodpeckerci/woodpecker-agent:latest";
                      cmd = [ "agent" ];
                      dependsOn = [ "woodpecker-server" ];
                      volumes = [
                        "/var/run/docker.sock:/var/run/docker.sock"
                        "${cfg.agentSecretFile}:/agentsecretfile:ro"
                      ];
                      environment = {
                        WOODPECKER_SERVER = "woodpecker-server:9000";
                        WOODPECKER_AGENT_SECRET_FILE = "/agentsecretfile";
                      } // cfg.extraConfig ;
                    };
                  };
                };
                podman = {
                  dockerSocket.enable = true;
                  enable = true;
                };
              };
            };
          };
      }
    );
}
