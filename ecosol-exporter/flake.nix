{
  inputs = {
    utils.url = "github:numtide/flake-utils";
  };
  outputs = { self, nixpkgs, utils }: utils.lib.eachDefaultSystem (system:
    let
      pkgs = nixpkgs.legacyPackages.${system};
      basePkg = pkgs.buildGoModule rec {
        pname = "ecosol-exporter";
        version = "0.1.0";
        src = pkgs.fetchFromGitHub {
          owner = "michcioperz";
          repo = "ecosol_exporter";
          rev = "main";
          sha256 = "158bb4pi2jhg39frhwqwr4y7l52mhm330xskpzq843w4v8px652a";
        };
        vendorSha256 = "0h9pm1nj52sf7bkl1n7cw7zrci8lw7y88hp03m9nxg9y4ja3ciih";
        meta = with pkgs.lib; {
          description = "Ecosol exporter for Prometheus";
          homepage = "https://github.com/michcioperz/ecosol_exporter";
          license = licenses.asl20;
          platforms = platforms.unix;
        };
      };
    in rec {
      nixosModules.ecosol-exporter = { config, lib, pkgs, ... }: let cfg = config.services.prometheus.exporters.ecosol; in {
        options.services.prometheus.exporters.ecosol = {
          enable = lib.mkEnableOption "";
          port = lib.mkOption {
            default = 8080;
            type = lib.types.port;
          };
          firewallFilter = lib.mkOption {
            default = null;
          };
          openFirewall = lib.mkOption {
            default = false;
          };
          url = lib.mkOption {
            example = "http://admin:admin@192.168.2.9/econet/regParams";
            type = lib.types.str;
          };
          bindAddr = lib.mkOption {
            default = ":${toString cfg.port}";
            type = lib.types.str;
          };
        };
        config = lib.mkIf cfg.enable {
          networking.firewall.allowedTCPPorts = lib.mkIf cfg.openFirewall [ cfg.port ];
          systemd.services.ecosol-exporter = {
            after = [ "network.target" ];
            serviceConfig = {
              DynamicUser = true;
              ExecStart = ''
                ${basePkg}/bin/ecosol_exporter --ecosol.address=${cfg.url} --listen-address=${cfg.bindAddr}
              '';
              Restart = lib.mkDefault "always";
              PrivateTmp = lib.mkDefault true;
              WorkingDirectory = lib.mkDefault "/tmp";
              User = lib.mkDefault "ecosol-exporter";
              Group = lib.mkDefault "ecosol-exporter";
            };
            wantedBy = [ "multi-user.target" ];
          };
        };
      };
      packages = {
        default = basePkg;
        ecosol-exporter = basePkg;
      };
    });
}
