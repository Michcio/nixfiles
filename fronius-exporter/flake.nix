{
  inputs = {
    utils.url = "github:numtide/flake-utils";
  };
  outputs = { self, nixpkgs, utils }: utils.lib.eachDefaultSystem (system:
    let
      pkgs = nixpkgs.legacyPackages.${system};
      basePkg = pkgs.buildGoModule rec {
        pname = "fronius-exporter";
        version = "0.9.0";
        src = pkgs.fetchFromGitHub {
          owner = "ccremer";
          repo = "fronius-exporter";
          rev = "v${version}";
          hash = "sha256-gWdHxSac8j900Lm107snOJiElR7L45YdlSJqnEAyHsw=";
        };
        vendorSha256 = "sha256-t9VeeSGa2R1bQRMjXbyI18b62HW78lJnuYMISKNXeyA=";
        meta = with pkgs.lib; {
          description = "Fronius exporter for Prometheus";
          homepage = "https://github.com/ccremer/fronius-exporter";
          license = licenses.asl20;
          platforms = platforms.unix;
        };
      };
    in rec {
      nixosModules.fronius-exporter = { config, lib, pkgs, ... }: let cfg = config.services.prometheus.exporters.fronius; in {
        options.services.prometheus.exporters.fronius = {
          enable = lib.mkEnableOption "";
          port = lib.mkOption {
            default = 8080;
            type = lib.types.port;
          };
          firewallFilter = lib.mkOption {
            default = null;
          };
          openFirewall = lib.mkOption {
            default = false;
          };
          symoUrl = lib.mkOption {
            example = "http://symo.ip.or.hostname/solar_api/v1/GetPowerFlowRealtimeData.fcgi";
            type = lib.types.str;
          };
          bindAddr = lib.mkOption {
            default = ":${toString cfg.port}";
            type = lib.types.str;
          };
        };
        config = lib.mkIf cfg.enable {
          networking.firewall.allowedTCPPorts = lib.mkIf cfg.openFirewall [ cfg.port ];
          systemd.services.fronius-exporter = {
            after = [ "network.target" ];
            serviceConfig = {
              DynamicUser = true;
              ExecStart = ''
                ${basePkg}/bin/fronius-exporter --symo.url=${cfg.symoUrl} --bind-addr=${cfg.bindAddr}
              '';
              Restart = lib.mkDefault "always";
              PrivateTmp = lib.mkDefault true;
              WorkingDirectory = lib.mkDefault "/tmp";
              User = lib.mkDefault "fronius-exporter";
              Group = lib.mkDefault "fronius-exporter";
            };
            wantedBy = [ "multi-user.target" ];
          };
        };
      };
      packages = {
        default = basePkg;
        fronius-exporter = basePkg;
      };
    });
}
