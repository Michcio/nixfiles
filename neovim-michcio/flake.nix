{
  description = "Michcio flavoured neovim";
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-22.05";
    utils.url = "github:numtide/flake-utils";
  };
  outputs = { self, nixpkgs, utils }:
    let
      defaultBackground = "dark";
    in
    utils.lib.eachDefaultSystem (system: 
      let
        pkgs = nixpkgs.legacyPackages.${system};
      in
      rec {
        nixosModules.neovim-michcio = { config, lib, pkgs, ... }:
          let
            cfg = config.programs.neovim-michcio;
            basePkg = localLib.neovim-michcio cfg;
            viPkg = localLib.neovim-michcio-vi cfg;
          in
        {
          options.programs.neovim-michcio = {
            enable = lib.mkEnableOption "install Michcio's neovim config";
            background = lib.mkOption {
              default = defaultBackground;
              type = lib.types.enum [ "light" "dark" ];
            };
            linkAsVi = lib.mkOption {
              default = true;
              type = lib.types.bool;
            };
          };
          config = lib.mkIf config.programs.neovim-michcio.enable {
            environment.systemPackages = [ basePkg ]
              ++ lib.optionals cfg.linkAsVi [ viPkg ];
          };
        };
        localLib = {
          neovim-michcio =
            { background
            , ...
            }:
            pkgs.neovim.override {
              configure = {
              customRC = ''
                set encoding=utf-8
                set background=${background}
                set nocompatible
                set updatetime=100
                set cmdheight=2
                set shortmess+=c
                set tabstop=2 softtabstop=2 shiftwidth=2 expandtab
                autocmd FileType go,make setlocal noexpandtab
                autocmd FileType python setlocal tabstop=4 softtabstop=4 shiftwidth=4
                set colorcolumn=80
                set modelines=1
                set number
                set cursorline
                set wildmenu
                set lazyredraw
                set showmatch incsearch hlsearch
                set laststatus=2
                colorscheme rose-pine
                set termguicolors
                let g:airline_powerline_fonts = 1
              '';
              plug.plugins = with pkgs.vimPlugins; [
                vim-airline
                vim-airline-themes
                vim-nix
                zig-vim
                rose-pine
              ];
            };
          };
          neovim-michcio-vi = attrs: pkgs.writeScriptBin "vi" ''
            #!${pkgs.stdenvNoCC.shell}
            exec ${localLib.neovim-michcio attrs}/bin/nvim "$@"
          '';
        };
        packages = rec {
          vim-selenized = pkgs.vimUtils.buildVimPluginFrom2Nix {
            pname = "vim-selenized";
            version = "2020-05-06";
            src = pkgs.fetchFromGitHub {
              owner = "jan-warchol";
              repo = "selenized";
              rev = "e93e0d9fb47c7485f18fa16f9bdb70c2ee7fb5db";
              hash = "sha256-r+MLxpT91YN2FCqUqhE8oSztlb4ZAdz6P98cLeF0th4=";
            } + "/editors/vim";
            meta.homepage = "https://github.com/jan-warchol/selenized";
          };
          neovim-michcio = localLib.neovim-michcio { background = defaultBackground; };
          default = neovim-michcio;
        };
      });
}
